import unittest
import logging
from dedup.processor import Processor
from dedup.context import ctx
from pathlib import Path


class Test(unittest.TestCase):
    def test_stats(self):
        ctx.dirs = [Path(r"C:\dedup\test_dir")]
        ctx.verbose = True
        ctx.dry_run = True
        if ctx.verbose:
            loglevel = logging.DEBUG
        else:
            loglevel = logging.INFO
        FORMAT = "'%(asctime)s %(levelname)s [%(filename)s:%(lineno)s - %(funcName)10s()]  %(message)s"
        logging.basicConfig(
            format=FORMAT, datefmt="%m/%d/%Y %I:%M:%S %p", level=loglevel
        )

        Processor(ctx.dirs).stats()
