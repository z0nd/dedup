from walker import Walker
from compare import Comparator


def refresh(ctx, dry_run, dirs):
    accoumulator = {}
    w = Walker(dry_run)
    for d in dirs:
        accoumulator.update(w.build(d))

    c = Comparator(accoumulator, dry_run)
    print(c.duplicates())


if __name__ == "__main__":
    refresh(None, False, ["C:\\ilya\\salo"])
    # import pyautogui
    # pyautogui.moveTo(100, 150)
    # pyautogui.moveRel(0, 10)  # move mouse 10 pixels down
