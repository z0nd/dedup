import io
from setuptools import setup, find_packages

with io.open("README.rst", "rt", encoding="utf8") as f:
    readme = f.read()


setup(
    name="dedup",
    version="0.0.2",
    license="BSD",
    author="Ilya Levin",
    author_email="worroc@zoho.com",
    maintainer="worroc",
    maintainer_email="worroc@zoho.com",
    description="deduplicate identical files",
    long_description=readme,
    packages=find_packages(),
    python_requires=">=3.8",
    install_requires=["click", "clickclick", "send2trash"],
    entry_points="""
        [console_scripts]
        dedup=main:cli
        tidy=tree:cli
    """,
)
