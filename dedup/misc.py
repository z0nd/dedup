import os

import send2trash
import clickclick as cc
from .context import ctx


def del_file(file_path: str):
    if not os.path.exists(file_path):
        return
    if ctx.verbose:
        cc.action(f"deleting {file_path}\n")
    if not ctx.dry_run:
        if ctx.unlink:
            os.unlink(file_path)
        else:
            send2trash.send2trash(file_path)
