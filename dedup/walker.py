import os
import sys
import itertools

import clickclick as cc

from .context import ctx
from .reader import File
from . import cache


spinner = itertools.cycle(["-", "/", "|", "\\"])


class Walker:
    def directories(self, dir_name: str):
        for current_dir, dirs, files in os.walk(dir_name):
            if os.path.basename(current_dir).startswith("."):
                continue
            yield current_dir

    def build(self, dir_name: str):
        """ wall through the FS and scan files
        return dict of all files
        """
        progress_file = None
        progress_data = set()
        if ctx.rerun:
            try:
                with ctx.progress_filename.open(encoding="utf-8", mode="rt") as fi:
                    progress_data = set([x.strip() for x in fi.readlines()])
            except FileNotFoundError:
                progress_data = set()
            progress_file = ctx.progress_filename.open(encoding="utf-8", mode="a")
        else:
            progress_file = ctx.progress_filename.open(encoding="utf-8", mode="w")

        try:
            counter = 0
            accomulator = {}
            directories = {}
            cc.info(f"reading file system {dir_name}")
            for current_dir, dirs, files in os.walk(dir_name):
                # process single directory
                if os.path.basename(current_dir).startswith("."):
                    continue

                old_cache = cache.load(current_dir)
                if cache.exists(current_dir):
                    if current_dir in progress_data:
                        directories[current_dir] = old_cache
                        accomulator.update(old_cache)
                        cc.warning(f"cached: {current_dir.encode('utf8')}")
                        continue
                cc.ok(current_dir.encode("utf8"))
                new_cache = cache.new(current_dir)
                cache_changed = False
                exception = False
                for file in files:
                    if file == ctx.cache_filename:
                        continue
                    counter += 1
                    filename = os.path.join(current_dir, file)

                    if filename in old_cache:
                        file_obj = File.from_cache(old_cache.get(filename))
                        cache_changed = cache_changed or not file_obj.hashed
                    else:
                        file_obj = File(filename, current_dir)
                        cache_changed = True
                    new_cache[filename] = file_obj

                    # only for create hash
                    try:
                        file_obj.ensure_hash()
                    except Exception as e:
                        cc.warning(f"unable to hash for file {filename}")
                        exception = True
                        continue

                if cache_changed:
                    if not exception:
                        if progress_file:
                            progress_file.write(current_dir + "\n")
                            progress_file.flush()
                    new_cache.store()

                directories[current_dir] = new_cache
                accomulator.update(new_cache)

            return accomulator, directories
        finally:
            if progress_file:
                progress_file.close()
