import os
from typing import List
from hashlib import md5
from dataclasses import dataclass
from pathlib import Path
from collections import defaultdict
import clickclick as cc

from .context import ctx

REMOVES = str
UNKOWNS = str


class Appraiser:
    def __init__(self):
        self._rules = defaultdict(int)  # file_path: weight
        if ctx.rerun and os.path.exists(ctx.appraiser_rules_filename):
            with open(ctx.appraiser_rules_filename, "r") as fi:
                for line in fi.readlines():
                    if not line.strip():
                        continue
                    # 1:filename
                    cc.info(line)
                    _wieght, _path = line.split(":")
                    self._rules[_path.strip()] = int(_wieght)

    def calc_weight(self, filepath):
        # calculates weight for cpeciefic filepath
        weight = 0
        for rule in self._rules:
            if filepath.startswith(rule):
                weight += self._rules[rule]
            if os.path.dirname(filepath) == rule:
                # increase wieght if rule exact match the file directory
                weight += self._rules[rule]
        return weight

    def decide(self, files) -> (List[UNKOWNS], List[REMOVES]):
        weighted = defaultdict(list)
        for filename in files:
            weighted[self.calc_weight(filename)].append(filename)
        sorted_by_weight = sorted(weighted.items(), reverse=True)
        selected = sorted_by_weight[0][1]
        leftovers = []
        for x in sorted_by_weight[1:]:
            leftovers += x[1]
        return selected, leftovers

    def add_from_file(self, file_path: str):
        dirname = os.path.dirname(file_path)
        self._rules[dirname] += 1
        with open(ctx.appraiser_rules_filename, "w") as fo:
            for _path, _weight in self._rules.items():
                fo.write(f"{_weight}:{_path}\n")
