from typing import List
from pathlib import Path
from dataclasses import dataclass, field


@dataclass
class RunContext:
    verbose: bool = False
    dry_run: bool = False
    rerun: bool = False
    dirs: List[Path] = field(default_factory=list)
    unlink: bool = False
    cache_filename: str = ".dedup-meta.cpl"
    progress_filename: Path = Path(".dedup.progress")
    appraiser_rules_filename: str = ".dedup.rules.list"


ctx = RunContext()
