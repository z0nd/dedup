from collections import defaultdict


class Comparator:
    def __init__(self, accoumulator: dict, dry_run: bool):
        self.accoumulator = accoumulator
        self.dry_run = dry_run

    def duplicates(self):
        hash_map = defaultdict(set)
        for name, file in self.accoumulator.items():
            hash_map[file.hash].add(name)
        return hash_map
