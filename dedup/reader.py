import os

from hashlib import md5


class File:
    def __init__(self, filename, directory):
        self.filename = filename
        self._stat = None
        self._hash = None
        self.directory = directory

    @property
    def hashed(self):
        return self._hash is not None

    def ensure_hash(self):
        self.hash

    @property
    def hash(self):
        if not self._hash:
            self._hash = FileReader.hash(self.filename)
        return self._hash

    @property
    def stat(self):
        if not self._stat:
            self._stat = FileReader.stat(self.filename)
        return self._stat

    @classmethod
    def from_cache(cls, other: "File"):
        f = cls(other.filename, other.directory)
        mst = f.stat
        ost = other.stat
        if (mst.st_size, round(mst.st_mtime, 2)) == (
            ost.st_size,
            round(ost.st_mtime, 2),
        ):
            f._hash = other._hash
        return f


class FileReader:
    @staticmethod
    def hash(filename):
        def _read_file_chunks(filename, fi, chunk_length=1024):
            while 1:
                chunk = fi.read(chunk_length)
                if not chunk:
                    return
                yield chunk

        with open(filename, "rb") as fi:
            m = md5()
            for chunk in _read_file_chunks(
                filename, fi, chunk_length=128
            ):  # 64 / 128 the best for updating md5
                m.update(chunk)
        return m.hexdigest()

    @staticmethod
    def stat(f):
        return os.stat(f)
