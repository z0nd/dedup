from typing import Dict, List, Tuple

from . import appraiser


import clickclick as cc

FileHash = str
FilePath = str
PathWeight = int


class Press:
    def __init__(self):
        self.appraiser = appraiser.Appraiser()

    def squeeze_redundant(self, dups: Dict[FileHash, List[FilePath]]) -> List[FilePath]:
        redundant_files = []
        total = len(dups.items())
        for index, (_md5, files) in enumerate(dups.items()):
            good_file, redundant_by_rules = self.appraiser.decide(files)
            redundant_files += redundant_by_rules
            if len(good_file) > 1:
                cc.info(f"file {index} from {total}")
                good_file, redundant = self.filter_by_biobot(good_file)
                self.appraiser.add_from_file(good_file)
                redundant_files += redundant
            # store good_file to file for future rerun
        return redundant_files

    def filter_by_biobot(self, files) -> (FilePath, List[FilePath]):
        # return rule, selected file and files to remove

        # question the user for keep rule
        cc.info("what do you want to keep?\n")

        questions = []
        cc.info("-. remove all\n")
        cc.info("+. leave all\n")
        for index, filename in enumerate(files):
            cc.info(f"{index}. {filename}\n")
            questions.append(str(index))
        questions += ["-", "+"]

        answer = ""
        while answer not in questions:
            answer = input("select>").lower()

        if answer == "-":
            return None, None, files
        elif answer == "+":
            return None, files, []

        keep_index = int(answer)
        files_to_delete = files[:keep_index] + files[keep_index + 1 :]

        return files[keep_index], files_to_delete
