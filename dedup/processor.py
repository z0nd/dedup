import os
import sys

from collections import defaultdict
from typing import Dict, List, Tuple
from pathlib import Path

from .walker import Walker
from .misc import del_file
from . import cache
from . import colander

import clickclick as cc

MAX_DEPTH = 256


class Processor:
    def __init__(self, dirs):
        super().__init__()
        self.dirs = dirs

        self.press = colander.Press()

    def clear_cache(self):
        w = Walker()
        for d in self.dirs:
            for d in w.directories(d):
                cache.clear(d)

    def calculus(self) -> Tuple[List, List]:
        # calculates a full tree and duplicates
        accoumulation = {}
        w = Walker()

        for d in self.dirs:
            files, directories = w.build(d)
            accoumulation.update(files)

        for dir_cache in directories.values():
            if dir_cache:
                dir_cache.store()

        duplicates = self._duplicates(accoumulation)

        return accoumulation, duplicates

    def stats(self):
        # display all
        files, dups = self.calculus()
        for md5, files in dups.items():
            cc.info(f"{md5}")
            for filename in files:
                cc.info(f"\t{filename}")

    def dedup(self):
        _files, dups = self.calculus()
        if not dups:
            cc.info(f"no duplicates")
            return
        files_to_delete = self.press.squeeze_redundant(dups)
        cc.info(f"processing files {len(files_to_delete)}\n")
        self._purge(files_to_delete, dups)

    def _purge(self, files_to_delete, dups):
        while True:
            answer = input(
                "do you want to remove {} files yes/no/list> ".format(
                    len(files_to_delete)
                )
            )
            if answer == "list":
                # let make reverse list and display that deleted and where to leave
                good_map = defaultdict(set)
                del_map = defaultdict(set)
                rev = {}
                for hash_id, files in dups.items():
                    for f in files:
                        rev[f] = hash_id

                for file_name in files_to_delete:
                    hs = rev[file_name]
                    del_map[hs].add(file_name)
                for hs, files in del_map.items():
                    good_map[hs] = set(dups[hs]) - files

                for hs, files in good_map.items():
                    cc.ok("\n".join(files))
                    for df in del_map[hs]:
                        cc.error(f"\t{df}")
                continue
            elif answer == "no":
                cc.info("no deletion.\n")
                break
            elif answer == "yes":
                for file_name in files_to_delete:
                    cc.info(f"removing f{file_name}")
                    del_file(file_name)

                # remove empty directories (skip cache file)
                dirs = set()
                for file in files_to_delete:
                    dirs.add(Path(file).parts[:-1])

                dirs = sorted(list(dirs), key=lambda x: len(x), reverse=True)
                for d in dirs:
                    directory = Path(*d)
                    cache.clear(directory)
                    if os.path.exists(directory) and not os.listdir(directory):
                        os.rmdir(directory)

                break
            else:
                cc.info("unknown input\f")

    def _duplicates(self, files):
        inv = defaultdict(list)
        for filename, file in files.items():
            try:
                inv[file.hash].append(filename)
            except Exception as e:
                sys.stderr.write(f"retrieving file hash {str(e)}")
        inv = {k: v for k, v in inv.items() if len(v) > 1}
        return inv
